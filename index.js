'use strict'; // eslint-disable-line strict,lines-around-directive

const fs = require('fs');
const path = require('path');
const walk = require('walk');

function plugin(options) {
    if (!('handlebars' in options)) {
        throw new Error('Please provide handlebars instance in options');
    }

    options.directory = options.directory || 'partials/';

    return (files, metalsmith, done) => {
        const walker = walk.walk(options.directory, {});

        walker.on('file', (root, fileStats, next) => {
            const filePath = path.normalize(`${root}/${fileStats.name}`);
            fs.readFile(filePath, 'utf8', (err, data) => {
                if (err) {
                    done(err);
                }
                const templateName = fileStats.name.split('.').shift();
                let currentPath = root.replace(options.directory, '').replace(/^[\\/]/, '');
                currentPath = currentPath === '' ? currentPath : `${currentPath}/`;
                options.handlebars.registerPartial(`${currentPath}${templateName}`, data);
                next();
            });
        });

        walker.on('errors', (root, stats, next) => {
            done(stats.error);
        });

        walker.on('end', () => {
            setImmediate(done);
        });
    };
}

module.exports = plugin;
