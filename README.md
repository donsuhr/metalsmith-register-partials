# metalsmith-register-partials

Metalsmith middleware to load a folder of partials into an existing handlebars instance.

##options

`handlebars` *required* provide a handlebars instance 
`directory` *required* directory of partials to read

```
... 
.use(generatePartials({
    handlebars,
    directory: 'app/html/partials',
}))
...
```
 
